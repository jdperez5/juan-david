package co.com.proyectobase.screenplay.stepdefinitions;


import org.openqa.selenium.WebDriver;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.GenerarReporteLunesyViernes;
import co.com.proyectobase.screenplay.tasks.GenerarReporteSabadoyDomingo;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import co.com.proyectobase.screenplay.tasks.LaRespuesta;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

public class TraductorGoogleStepDefinition {
	
    @Managed(driver="chrome")
	private WebDriver hisBrowser;
    private Actor Juan = Actor.named("Juan");
	
	@Before
	public void configuracionInicial(){
		Juan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	
	@Given("^que Juan quiere usar$")
	public void queJuanQuiereUsar() {
	   Juan.wasAbleTo(Abrir.LaPaginaDeMaxtime());
	}

	
	
	@Given("^el ingresa el usuario \"([^\"]*)\" y la contrasena \"([^\"]*)\"$")
	public void elIngresaElUsuarioYLaContrasena(String palabra, String palabra2){
		  Juan.attemptsTo(Ingresar.DiaLaboralenSemana(palabra, palabra2));
	}
	
	
	@Given("^el usuario ve la palabra deseada \"([^\"]*)\"$")
	public void elUsuarioVeLaPalabraDeseada(String arg1) throws Exception {
		Juan.should(seeThat(LaRespuesta.es(), equalTo(arg1)));
	}

	  @When("^el configura su dia seleccionando \"([^\"]*)\" en el campo servicio y el Comentario \"([^\"]*)\"$")
	  public void elConfiguraSuDiaSeleccionandoEnElCampoServicio(String Palabra,String Comentario) throws Exception {
		  Juan.attemptsTo(GenerarReporteLunesyViernes.Configurardia(Palabra,Comentario));
	  }


	  @When("^el usuario ciera sabado y domingo$")
	  public void elUsuarioCieraSabadoYDomingo() throws Exception {
	      Juan.attemptsTo(GenerarReporteSabadoyDomingo.Cerrar());
	  }



	  
	  

}
