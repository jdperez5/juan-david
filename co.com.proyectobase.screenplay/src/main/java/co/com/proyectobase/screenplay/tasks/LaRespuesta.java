package co.com.proyectobase.screenplay.tasks;


import co.com.proyectobase.screenplay.model.Validarentrada;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta implements Question<String> {

	

	public static LaRespuesta es() {
		// TODO Auto-generated method stub
		return new LaRespuesta();
	}

	@Override
	public String answeredBy(Actor actor) {
	
		return Text.of(Validarentrada.VALIDA_ENTRADA).viewedBy(actor).asString();
		
	}
}
