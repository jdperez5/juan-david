package co.com.proyectobase.screenplay.tasks;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.model.CrearDia;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class GenerarReporteSabadoyDomingo implements Task {
	private static WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.FILTRO_USUARIO));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.FILTRO_SELECCIONAR_USUARIO));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.BOTON_DIA));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.OPCION_CERRAR_DIA));
		actor.attemptsTo(Esperar.aMoment());
		driver.switchTo().frame(0);
		actor.attemptsTo(Click.on(CrearDia.OPCION_CERRAR_DIA2));
		driver.switchTo().defaultContent();
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.BOTON_DIA));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.OPCION_CERRAR_DIA));
		actor.attemptsTo(Esperar.aMoment());
		driver.switchTo().frame(0);
		actor.attemptsTo(Click.on(CrearDia.OPCION_CERRAR_DIA2));
		driver.switchTo().defaultContent();
	}

	public static GenerarReporteSabadoyDomingo Cerrar() {
		
		return Tasks.instrumented(GenerarReporteSabadoyDomingo.class);
	}
}
