package co.com.proyectobase.screenplay.tasks;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.model.CrearDia;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

  

public class GenerarReporteLunesyViernes  implements Task  {
	private static WebDriver driver = Serenity.getWebdriverManager().getCurrentDriver();
	
	public GenerarReporteLunesyViernes(String Palabra,String Comentario) {
		super();
		this.Palabra = Palabra;
		this.Comentario = Comentario;
	}

	private String Palabra;
	private String Comentario;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.FILTRO_USUARIO));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.FILTRO_SELECCIONAR_USUARIO));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.BOTON_DIA));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Click.on(CrearDia.BOTON_NUEVO_REPORTE));
		actor.attemptsTo(Esperar.aMoment());
		actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(CrearDia.BOTON_BUSCAR_PROYECTO));
	    actor.attemptsTo(Esperar.aMoment());
	    driver.switchTo().frame(0);
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_PROYECTO));
	    driver.switchTo().defaultContent();
	    actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(CrearDia.TIPO_DE_HORA));
	    actor.attemptsTo(Click.on(CrearDia.HORA_DE_PROYECTO));
	    actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_SERVICIO));
	    actor.attemptsTo(Esperar.aMoment());
	    driver.switchTo().frame(0);
	    actor.attemptsTo(Enter.theValue(Palabra).into(CrearDia.OPCION_INGRESAR_SERVICIO));
	    actor.attemptsTo(Click.on(CrearDia.OPCION_BUSCAR_SERVICIO));
	    actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_AGILES));
	    driver.switchTo().defaultContent();
	    actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(CrearDia.OPCION_ACTIVIDAD));
	    actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_ACTIVIDAD));
	    actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_HORAS));
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_HORAS));
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_HORAS));
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_HORAS));
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_HORAS));
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_HORAS));
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_HORAS));
	    actor.attemptsTo(Click.on(CrearDia.OPCION_SELECCIONAR_HORAS));
	    actor.attemptsTo(Enter.theValue(Comentario).into(CrearDia.OPCION_INGRESAR_COMENTARIO));
	    actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(CrearDia.OPCION_GUARDAR_Y_CERRAR_DIA));
	    actor.attemptsTo(Esperar.aMoment());
	    actor.attemptsTo(Click.on(CrearDia.OPCION_CERRAR_DIA));
	    actor.attemptsTo(Esperar.aMoment());
	    driver.switchTo().frame(0);
	    actor.attemptsTo(Click.on(CrearDia.OPCION_CERRAR_DIA2));
	    driver.switchTo().defaultContent();
	    
	/*	
		actor.attemptsTo(Click.on(CrearDia.BOTON_NUEVO_REPORTE));
		//DESPUES DE DAR EN EL BOTON NUEVO LO ACTIVO
		driver.switchTo().frame(0);
		//despues de que se cierra la pantalla
		driver.switchTo().defaultContent();
		
		*/
	}

	
	public static GenerarReporteLunesyViernes Configurardia(String Palabra, String Comentario) {
	
		return Tasks.instrumented(GenerarReporteLunesyViernes.class,Palabra,Comentario);
		
		
	}
}
