package co.com.proyectobase.screenplay.tasks;
import co.com.proyectobase.screenplay.model.MaxtimeHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Ingresar implements Task {

	public Ingresar(String usuario,String pass) {
		super();
		this.usuario = usuario;
		this.pass =pass;
		
	}
   
	private String usuario;
	 private String pass;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Enter.theValue(usuario).into(MaxtimeHomePage.CAMPO_USUARIO));
		actor.attemptsTo(Enter.theValue(pass).into(MaxtimeHomePage.CAMPO_CONTRASENA));
		actor.attemptsTo(Click.on(MaxtimeHomePage.BOTON_CONECTARSE));
		
	}

	public static Ingresar DiaLaboralenSemana(String usuario,String pass) {
		return Tasks.instrumented(Ingresar.class,usuario,pass);
		
	}
	
	
	
}
