package co.com.proyectobase.screenplay.model;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.choucairtesting.com:18000/MaxTimeCHC/Login.aspx?ReturnUrl=%2fMaxtimeCHC%2f")

public class MaxtimeHomePage extends PageObject{
	
	public static final Target CAMPO_USUARIO = Target.the("el campo de usuario")
			.located(By.xpath("//INPUT[@id='Logon_v0_MainLayoutEdit_xaf_l30_xaf_dviUserName_Edit_I']"));
		
	public static final Target CAMPO_CONTRASENA = Target.the("el campo de contraseņa")
			.located(By.xpath("//INPUT[@id='Logon_v0_MainLayoutEdit_xaf_l35_xaf_dviPassword_Edit_I']"));
	
	public static final Target BOTON_CONECTARSE = Target.the("el boton conectarse")
			.located(By.xpath("//A[@title='Conectarse'][text()='Conectarse']"));
}
