package co.com.proyectobase.screenplay.model;
 
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;

import net.serenitybdd.screenplay.targets.Target;

public class CrearDia extends PageObject {
	
	
	public static final Target FILTRO_USUARIO = Target.the("el Boton que muestra el dia laborado")
			.located(By.xpath("//*[@id='Vertical_v1_LE_v2_col1']//img[@alt='[Filtrar]']"));
	
	public static final Target FILTRO_SELECCIONAR_USUARIO = Target.the("el Boton que muestra el dia laborado")
			.located(By.xpath("//*[@id=\"Vertical_v1_LE_v2_HFListBox_LBI3T0\"]"));
	
	public static final Target BOTON_DIA = Target.the("el Boton que muestra el dia laborado")
			.located(By.xpath("//*[@id='Vertical_UPVSC']//table[@id='Vertical_v1_LE_v2_DXMainTable']/tbody/tr/td/table[contains(@id,'Fecha')]"));
	    
	public static final Target BOTON_NUEVO_REPORTE = Target.the("el boton que crea un nuevo reporte")
			.located(By.xpath("//*[@id='Vertical_v3_MainLayoutView_xaf_l103_xaf_dviReporteDetallado_UPToolBar']//li[contains(@id,'DXI0')]"));
	
	
	public static final Target BOTON_BUSCAR_PROYECTO = Target.the("el boton que busca un nuevo reporte")
			.located(By.xpath("//*[@id=\"Vertical_v11_MainLayoutEdit_xaf_l135_xaf_dviProyecto_Edit_Find_BImg\"]"));
	
	public static final Target OPCION_SELECCIONAR_PROYECTO = Target.the("el boton selecciona un proyecto")
			.located(By.xpath("//*[@id=\"Dialog_v6_LE_v7_cell1_0_xaf_Nombre_View\"]"));
	          

	public static final Target TIPO_DE_HORA = Target.the("el boton selecciona un tipo de hora")
			.located(By.xpath("//*[@id=\"Vertical_v11_MainLayoutEdit_xaf_l155_xaf_dviTipoHora_Edit_DD_B-1Img\"]"));

	
	public static final Target HORA_DE_PROYECTO = Target.the("el boton selecciona un tipo de hora")
			.located(By.xpath("//*[@id=\"Vertical_v11_MainLayoutEdit_xaf_l155_xaf_dviTipoHora_Edit_DD_DDD_L_LBI4T0\"]"));
	
	public static final Target OPCION_SELECCIONAR_SERVICIO = Target.the("el boton selecciona un servicio")
			.located(By.xpath("//*[@id='Vertical_v11_MainLayoutEdit_xaf_l160_xaf_dviServicio_Edit_Find_BImg']"));
	
	public static final Target OPCION_INGRESAR_SERVICIO = Target.the("el boton ingresa un servicio")
			.located(By.xpath("//*[@id=\"Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_Ed\"]/tbody/tr/td[1][@class ='dxic']/input"));
	
	
	public static final Target OPCION_BUSCAR_SERVICIO = Target.the("el boton busca un servicio")
			.located(By.xpath("//*[@id=\"Dialog_SearchActionContainer_Menu_ITCNT0_xaf_a0_B_CD\"]/span"));
	
	
	public static final Target OPCION_SELECCIONAR_AGILES = Target.the("el boton selecciona especifiamente")
			.located(By.xpath("//*[@class='WebEditorCell']/span[@id='Dialog_v8_LE_v9_cell1_0_xaf_Nombre_View']"));
	
	public static final Target OPCION_ACTIVIDAD = Target.the("el boton selecciona una actividad")
			.located(By.xpath("//*[@id=\"Vertical_v11_MainLayoutEdit_xaf_l165_xaf_dviActividad_Edit_DD_B-1\"]/table"));
	
	public static final Target OPCION_SELECCIONAR_ACTIVIDAD = Target.the("el boton selecciona una actividad")
			.located(By.xpath("//*[@id=\"Vertical_v11_MainLayoutEdit_xaf_l165_xaf_dviActividad_Edit_DD_DDD_L_LBI19T0\"]"));
	
	public static final Target OPCION_SELECCIONAR_HORAS = Target.the("el boton selecciona 8 horas")
			.located(By.xpath("//*[@id=\"Vertical_v11_MainLayoutEdit_xaf_l189_xaf_dviHoras_Edit_B-2Img\"][@alt='+']"));
	
	public static final Target OPCION_INGRESAR_COMENTARIO = Target.the("el textfield se ingresa un comentario")
			.located(By.xpath("//*[@id=\"Vertical_v11_MainLayoutEdit_xaf_l214_xaf_dviComentario_Edit_I\"][@type=\"text\"]"));
	
	public static final Target OPCION_GUARDAR_Y_CERRAR_DIA = Target.the("el boton guardar reporte")
			.located(By.xpath("//*[@id=\"Vertical_EditModeActions2_Menu_DXI1_T\"]/a"));
	
	public static final Target OPCION_CERRAR_DIA = Target.the("el boton cerrar dia")
			.located(By.xpath("//*[@id=\"Vertical_TB_Menu_DXI1_\"]"));
	
	public static final Target OPCION_CERRAR_DIA2 = Target.the("el aceptar finalizar dia")
			.located(By.xpath("//*[@id=\"Dialog_actionContainerHolder_Menu_DXI0_T\"]"));



	
	
}
