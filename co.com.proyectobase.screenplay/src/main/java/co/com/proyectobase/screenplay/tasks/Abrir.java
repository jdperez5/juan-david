package co.com.proyectobase.screenplay.tasks;


import co.com.proyectobase.screenplay.model.MaxtimeHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task{

	private MaxtimeHomePage maxtime;
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(maxtime));
	}

	public static Abrir LaPaginaDeMaxtime() {
		// TODO Auto-generated method stub
		return Tasks.instrumented(Abrir.class);
	}

}
